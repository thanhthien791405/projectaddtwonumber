from django.urls import path
from .views import MyBigNumber

urlpatterns = [
    path("sumnumber/", MyBigNumber.as_view(), name="sum"),
]