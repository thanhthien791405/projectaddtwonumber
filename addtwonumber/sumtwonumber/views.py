from django.shortcuts import render
from rest_framework.views import APIView

# Create your views here.
from .services import SumNumber


class MyBigNumber(APIView):
    def post(self, request):
        numberOne = request.data.get("number_one")
        numberTwo = request.data.get("number_two")

        return SumNumber(numberOne, numberTwo)
