from django.apps import AppConfig


class SumtwonumberConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "sumtwonumber"
