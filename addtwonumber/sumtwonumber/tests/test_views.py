from django.test import SimpleTestCase
from rest_framework import status
from django.urls import reverse


class TestSumTwoNumber(SimpleTestCase):
    def test_sum_two_number(self):
        sample_number = {"number_one": "123", "number_two": "4456"}
        response = self.client.post(reverse('sum'), sample_number)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_number_one_is_empty(self):
        sample_number = {"number_one": "", "number_two": "456"}
        response = self.client.post(reverse('sum'), sample_number)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_number_one_contains_the_characters(self):
        sample_number = {"number_one": "123a", "number_two": "456"}
        response = self.client.post(reverse('sum'), sample_number)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_number_two_is_empty(self):
        sample_number = {"number_one": "123", "number_two": ""}
        response = self.client.post(reverse('sum'), sample_number)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_number_two_contains_the_characters(self):
        sample_number = {"number_one": "123", "number_two": "123a"}
        response = self.client.post(reverse('sum'), sample_number)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
