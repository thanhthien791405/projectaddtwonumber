from django.test import SimpleTestCase
from rest_framework import status
from django.urls import reverse, resolve
from sumtwonumber.views import MyBigNumber


class TestUrls(SimpleTestCase):
    def test_url_is_resolved(self):
        url = reverse("sum")
        self.assertEqual(resolve(url).func.view_class, MyBigNumber)
