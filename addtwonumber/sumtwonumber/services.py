from rest_framework import status
from rest_framework.response import Response
from rest_framework.status import *


def SumNumber(number_one: str, number_two: str):
    number_one = number_one.replace(" ", "")
    number_two = number_two.replace(" ", "")
    if not len(number_one) or not len(number_two):
        return Response(data={
            "title": "Error",
            "messenger": ("Number one can't be left empty and must be number" if not number_one.isdigit() else "Number two can't be left empty and must be number"),
            "field_error": [("number_one" if not len(number_one) else "number_two")]
        }, status=status.HTTP_400_BAD_REQUEST)
    elif not number_one.isdigit() or not number_two.isdigit():
        return Response(data={
            "title": "Error",
            "messenger": ("Number one must not contain the characters" if not number_one.isdigit() else "Number two must not contain the characters"),
            "field_error": [("number_one" if not number_one.isdigit() else "number_two")]
        }, status=status.HTTP_400_BAD_REQUEST)

    if len(number_one) < len(number_two):
        number_one = number_one.rjust(len(number_two) - len(number_one) + len(number_one), " ")
    else:
        number_two = number_two.rjust(len(number_one) - len(number_two) + len(number_two), " ")

    result = ""
    number_temp = 0
    for i in range(len(number_one) - 1, -1, -1):
        if number_one[i] == " " or number_two[i] == " ":
            if number_one[i] == " " and i != 0:
                result = result.rjust(len(result) + 1, str((int(number_two[i]) + number_temp) % 10) if int(
                    number_two[i]) + number_temp > 9 else str(int(number_two[i]) + number_temp))
                number_temp = 1 if int(number_two[i]) + number_temp > 9 else 0
            elif number_one[i] == " " and i == 0:
                result = result.rjust(len(result) + 1, number_two[i]) if not number_temp else str(
                    int(number_two[i]) + number_temp) + result
            elif number_two[i] == " " and i != 0:
                result = result.rjust(len(result) + 1, str((int(number_one[i]) + number_temp) % 10) if int(
                    number_one[i]) + number_temp > 9 else str(int(number_one[i]) + number_temp))
                number_temp = 1 if int(number_one[i]) + number_temp > 9 else 0
            else:
                result = result.rjust(len(result) + 1, number_one[i]) if not number_temp else str(
                    int(number_one[i]) + number_temp) + result
        elif int(number_one[i]) + int(number_two[i]) > 9 and i != 0:
            result = result.rjust(len(result) + 1, str(((int(number_one[i]) + int(number_two[i]) + number_temp) % 10)))
            number_temp = number_temp + 1 if not number_temp else number_temp
        elif int(number_one[i]) + int(number_two[i]) + number_temp > 9 and i != 0:
            result = result.rjust(len(result) + 1, str(((int(number_one[i]) + int(number_two[i]) + number_temp) % 10)))
            number_temp = number_temp + 1 if not number_temp else number_temp
        elif int(number_one[i]) + int(number_two[i]) + number_temp > 9 and i == 0:
            result = str(int(number_one[i]) + int(number_two[i]) + number_temp) + result
        else:
            result = result.rjust(len(result) + 1, str(int(number_one[i]) + int(number_two[i]) + number_temp))
            number_temp = 0
    data = {
        "number_one": number_one.replace(" ", ""),
        "number_two": number_two.replace(" ", ""),
        "result": result
    }
    return Response(data=data, status=HTTP_200_OK)
