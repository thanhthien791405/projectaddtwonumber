install requirements
```bash
pip install -r requirements.txt
```

run test automation
```bash
python manage.py test sumtwonumber
```

run project
```bash
python manage.py runserver
```

API testing
```bash
url: localhost/sumnumber/
method: post
body: type json
fields body: number_one, number_two
example: {
            'number_one': "123", 
            'number_two': "456"
        }
```